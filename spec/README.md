# Tor specifications

These specifications describe how Tor works.  They try to present
Tor's protocols in sufficient detail to allow the reader to implement
a compatible implementation of Tor without ever having to read the Tor
source code.

They were once a separate set of text files, but in late 2023 we
migrated to use [`mdbook`](https://rust-lang.github.io/mdBook/).
We're in the process of updating these documents to improve their quality.

This is a living document: we are always changing and improving them
in order to make them easier and more accurate, and to improve the
quality of the Tor protocols.  They are maintained as a set of
documents in a
[gitlab repository](https://gitlab.torproject.org/tpo/core/torspec/);
you can use that repository to see their history.

Additionally, the <a href="./proposals/index.html">proposals</a>
directory holds our design proposals.  These include historical
documents that have now been merged into the main specifications, and
new proposals that are still under discussion.  Of particular
interrest are the
<a href="./proposals/BY_STATUS.html#finished-proposals-implemented-specs-not-merged">`FINISHED`
Tor proposals</a>: They are the ones that have been implemented, but
not yet merged into these documents.

## Getting started

There's a lot of material here, and it's not always as well organized
as we like.  We have broken it into a few major sections.

For a table of contents, click on the menu icon to the top-left of
your browser scene.  You should probably start by reading [the core
tor protocol specification](./tor-spec), which describes how our
protocol works.  After that, you should be able to jump around to
the topics that interest you most.  The introduction of each top-level
chapter should provide an introduction.
______________________________________________________________________

[git repository]: https://gitlab.torproject.org/tpo/core/torspec/
[heading ids]: https://github.com/raphlinus/pulldown-cmark/blob/master/specs/heading_attrs.txt
[mdbook documentation]: https://rust-lang.github.io/mdBook/format/summary.html
[`spec/tor-spec/flow-control.md`]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/spec/tor-spec/flow-control.md?ref_type=heads
[`spec` directory]: https://gitlab.torproject.org/tpo/core/torspec/-/tree/main/spec?ref_type=heads
[`summary.md`]: https://gitlab.torproject.org/tpo/core/torspec/-/raw/main/spec/SUMMARY.md?ref_type=heads
