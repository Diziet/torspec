<a id="dir-spec.txt-3"></a>

# Directory authority operation and formats

Every authority has two keys used in this protocol:

`KP_auth_id_rsa`: a long-term RSA authority identity key.
This key SHOULD be at least 2048 bits long;
it MUST NOT be shorter than 1024 bits.

`KP_auth_sign_rsa`: a medium-term RSA authority signing key.
This key SHOULD be at least 2048 bits long;
it MUST NOT be shorter than 1024 bits.

The identity key `KP_auth_id_rsa` is used from time to time
to sign new key certificates
containing (and authenticating) new `KP_auth_sign_rsa` signing keys;
it is very sensitive.
It may be kept offline.

The signing key `KP_auth_sign_rsa` is used to sign consensuses, votes,
and similar documents.

(Authorities also have a router identity key `KP_relayid_rsa`,
and other keys used in their role as a router, and by earlier versions of the
directory protocol.)
