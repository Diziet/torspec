<a id="dir-spec.txt-2.1.3"></a>

# Nonterminals in server descriptors

nickname ::= between 1 and 19 alphanumeric characters (\[A-Za-z0-9\]),
case-insensitive.

hexdigest ::= a '$', followed by 40 hexadecimal characters
(\[A-Fa-f0-9\]) encoding a relay's `SHA1(DER(KP_relayid_rsa))`.

exitpattern ::= addrspec ":" portspec

portspec ::= "\*" | port | port "-" port

port ::= an integer between 1 and 65535, inclusive.

(Some implementations incorrectly generate ports with value 0.
Implementations SHOULD accept this, and SHOULD NOT generate it.
Connections to port 0 are never permitted.)

addrspec ::= "\*" | ip4spec | ip6spec

ipv4spec ::= ip4 | ip4 "/" num_ip4_bits | ip4 "/" ip4mask

ip4 ::= an IPv4 address in dotted-quad format

ip4mask ::= an IPv4 mask in dotted-quad format

num\_ip4\_bits ::= an integer between 0 and 32

ip6spec ::= ip6 | ip6 "/" num_ip6_bits

ip6 ::= an IPv6 address, surrounded by square brackets.

num\_ip6\_bits ::= an integer between 0 and 128

bool ::= "0" | "1"
