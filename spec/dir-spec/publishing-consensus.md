# Publishing the signed consensus { #publishing-signed-consensus }

The voting period ends at the valid-after time. If the consensus has
been signed by a majority of authorities, these documents are made
available at

`http://<hostname>/tor/status-vote/current/consensus.z`

and

`http://<hostname>/tor/status-vote/current/consensus-signatures.z`

```text
   [XXX current/consensus-signatures is not currently implemented, as it
    is not used in the voting protocol.]

   [XXX possible future features include support for downloading old
    consensuses.]

   The other vote documents are analogously made available under

http://<hostname>/tor/status-vote/current/authority.z
http://<hostname>/tor/status-vote/current/<fp>.z
http://<hostname>/tor/status-vote/current/d/<d>.z
http://<hostname>/tor/status-vote/current/bandwidth.z
```

once the voting period ends, regardless of the number of signatures.

The authorities serve another consensus of each flavor "F" from the
locations

```text
/tor/status-vote/(current|next)/consensus-F.z. and
/tor/status-vote/(current|next)/consensus-F/<FP1>+....z.
```

The standard URLs for bandwidth list files first-appeared in Tor 0.3.5.
