# Historical special hostnames

## .noconnect

SYNTAX:  \[string\].noconnect

When Tor sees an address in this format,
it immediately closes the connection without attaching it to any circuit.
This is useful for controllers that want to test
whether a given application is indeed using the same instance of Tor
that they're controlling.

This feature was added in Tor 0.1.2.4-alpha,
and taken out in Tor 0.2.2.1-alpha
over fears that it provided another avenue for detecting
Tor users via application-level web tricks.

## .onion (version 2)

```text
  SYNTAX:  [digest].onion
           [ignored].[digest].onion
```

In version 2 .onion addresses,
(removed along with the rest of v2 onion services in 0.4.5.11)
the "digest" part of the address
is the first eighty bits of a SHA-1 digest of
the hidden service's identity key
(`SHA1(DER(PK))`),
encoded in base 32.

The "ignored" portion of the address is intended for use in vhosting,
and is supported in Tor 0.2.4.10-alpha and later.

<!-- "PK" is not a good name, but rend-spec-v2.txt is archived. -->
