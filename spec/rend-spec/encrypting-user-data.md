<a id="rend-spec-v3.txt-5"></a>

# Encrypting data between client and host

A successfully completed handshake, as embedded in the
INTRODUCE/RENDEZVOUS messages, gives the client and hidden service host
a shared set of keys Kf, Kb, Df, Db, which they use for sending
end-to-end traffic encryption and authentication as in the regular
Tor relay encryption protocol, applying encryption with these keys
before other encryption, and decrypting with these keys before other
decryption. The client encrypts with Kf and decrypts with Kb; the
service host does the opposite.

As mentioned
[previously](./introduction-protocol.md#INTRO-HANDSHAKE-REQS),
these keys are used the same as for
[regular relay cell encryption](../tor-spec/routing-relay-cells.md),
except that instead of using AES-128 and SHA1,
both parties use AES-256 and SHA3-256.
