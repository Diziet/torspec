<a id="srv-spec.txt-4"></a>

# Specification {#spec}

<a id="srv-spec.txt-4.1"></a>

## Voting

This section describes how commitments, reveals and SR values are encoded in
votes. We describe how to encode both the authority's own
commitments/reveals and also the commitments/reveals received from the other
authorities. Commitments and reveals share the same line, but reveals are
optional.

Participating authorities need to include the line:

"shared-rand-participate"

in their votes to announce that they take part in the protocol.

<a id="srv-spec.txt-4.1.1"></a>

### Computing commitments and reveals {#COMMITREVEAL}

A directory authority that wants to participate in this protocol needs to
create a new pair of commitment/reveal values for every protocol
run. Authorities SHOULD generate a fresh pair of such values right before the
first commitment phase of the day (at 00:00UTC).

The value REVEAL is computed as follows:

```text
REVEAL = base64-encode( TIMESTAMP || SHA3_256(RN) )

      where RN is the SHA3-256 hashed value of a 256-bit random value. We hash the
      random value to avoid exposing raw bytes from our PRNG to the network (see
      [RANDOM-REFS]).

      TIMESTAMP is an 8-bytes network-endian time_t value. Authorities SHOULD
      set TIMESTAMP to the valid-after time of the vote document they first plan
      to publish their commit into (so usually at 00:00UTC, except if they start
      up in a later commit round).

   The value COMMIT is computed as follows:

      COMMIT = base64-encode( TIMESTAMP || SHA3_256(REVEAL) )
```

<a id="srv-spec.txt-4.1.2"></a>

### Validating commitments and reveals {#VALIDATEVALUES}

Given a COMMIT message and a REVEAL message it should be possible to verify
that they indeed correspond. To do so, the client extracts the random value
`SHA3_256(RN)` from the REVEAL message, hashes it, and compares it with the `SHA3_256(SHA3_256(RN))`
from the COMMIT message. We say that the COMMIT and REVEAL messages
correspond, if the comparison was successful.

<!-- TODO: The above paragraph seems wrong; there is no SHA3_256(SHA3_256(RN)
    in the commit message.
    -->

Participants MUST also check that corresponding COMMIT and REVEAL values
have the same timestamp value.

Authorities should ignore reveal values during the Reveal Phase that don't
correspond to commit values published during the Commitment Phase.

<a id="srv-spec.txt-4.1.4"></a>

### Encoding commit/reveal values in votes {#COMMITVOTE}

An authority puts in its vote the commitments and reveals it has produced and
seen from the other authorities. To do so, it includes the following in its
votes:

"shared-rand-commit" SP VERSION SP ALGNAME SP IDENTITY SP COMMIT \[SP REVEAL\] NL

where VERSION is the version of the protocol the commit was created with.
IDENTITY is the authority's SHA-1 identity fingerprint
(`HEX(SHA1(DER(KP_auth_id_rsa)))`)
and COMMIT is the
encoded commit \[COMMITREVEAL\].  Authorities during the reveal phase can
also optionally include an encoded reveal value REVEAL.  There MUST be only
one line per authority else the vote is considered invalid. Finally, the
ALGNAME is the hash algorithm that should be used to compute COMMIT and
REVEAL which is "sha3-256" for version 1.

<a id="srv-spec.txt-4.1.5"></a>

### Shared Random Value {#SRVOTE}

Authorities include a shared random value (SRV) in their votes using the
following encoding for the previous and current value respectively:

```text
     "shared-rand-previous-value" SP NUM_REVEALS SP VALUE NL
     "shared-rand-current-value" SP NUM_REVEALS SP VALUE NL
```

where VALUE is the actual shared random value encoded in hex (computed as
specified in section \[SRCALC\]. NUM_REVEALS is the number of reveal values
used to generate this SRV.

To maintain consistent ordering, the shared random values of the previous
period should be listed before the values of the current period.

<a id="srv-spec.txt-4.2"></a>

## Encoding Shared Random Values in the consensus {#SRCONSENSUS}

Authorities insert the two active shared random values in the consensus
following the same encoding format as in \[SRVOTE\].

<a id="srv-spec.txt-4.3"></a>

## Persistent state format {#STATEFORMAT}

As a way to keep ground truth state in this protocol, an authority MUST
keep a persistent state of the protocol. The next sub-section suggest a
format for this state which is the same as the current state file format.

It contains a preamble, a commitment and reveal section and a list of
shared random values.

The preamble (or header) contains the following items. They MUST occur in
the order given here:

"Version" SP version NL

\[At start, exactly once.\]

A document format version. For this specification, version is "1".

"ValidUntil" SP YYYY-MM-DD SP HH:MM:SS NL

\[Exactly once\]

```text
        After this time, this state is expired and shouldn't be used nor
        trusted. The validity time period is till the end of the current
        protocol run (the upcoming noon).
```

The following details the commitment and reveal section. They are encoded
the same as in the vote. This makes it easier for implementation purposes.

"Commit" SP version SP algname SP identity SP commit \[SP reveal\] NL

\[Exactly once per authority\]

The values are the same as detailed in section \[COMMITVOTE\].

This line is also used by an authority to store its own value.

Finally is the shared random value section.

"SharedRandPreviousValue" SP num_reveals SP value NL

\[At most once\]

```text
        This is the previous shared random value agreed on at the previous
        period. The fields are the same as in section [SRVOTE].

     "SharedRandCurrentValue" SP num_reveals SP value NL

        [At most once]

        This is the latest shared random value. The fields are the same as in
        section [SRVOTE].
```
