# Tor Protocol Specification

Note: This document aims to specify Tor as currently implemented, though it
may take it a little time to become fully up to date. Future versions of Tor
may implement improved protocols, and compatibility is not guaranteed.
We may or may not remove compatibility notes for other obsolete versions of
Tor as they become obsolete.

This specification is not a design document; most design criteria
are not examined.  For more information on why Tor acts as it does,
see tor-design.pdf.

