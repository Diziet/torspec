# Vanguards-Lite

Vanguards-Lite is meant for short-lived onion services such as OnionShare, as
well as for onion client activity.

It is designed for clients and services that expect to have an uptime of no
longer than 1 month.

## Design

Because it is for short-lived activity, its rotation times are chosen with the
Sybil adversary in mind, using the [max(X,X) skewed
distribution](./vanguards-stats.md#MaxDist).

We let NUM_LAYER2_GUARDS=4. We also introduce a consensus parameter
`guard-hs-l2-number` that controls the number of layer2 guards (with a
maximum of 19 layer2 guards).

No third layer of guards is used.

We don't write guards on disk. This means that the guard topology resets
when tor restarts.

## Rotation Period

The Layer2 lifetime uses the max(x,x) distribution with a minimum of one day
and maximum of 22 days. This makes the average lifetime of approximately two
weeks. Significant extensions of this lifetime are not recommended, as they
will shift the balance in favor of coercive attacks.

From the [Sybil Rotation Table](./vanguards-stats.md#SybilTable),
with NUM_LAYER2_GUARDS=4 it can be seen that this means that the Sybil attack
on Layer2 will complete with 50% chance in 18\*14 days (252 days) for the 1%
adversary, 4\*14 days (two months) for the 5% adversary, and 2\*14 days (one
month) for the 10% adversary.
